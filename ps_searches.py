import json

class PsSearchBundle(object):

    def create_simple_search_1(self, name, folderId):
        return {
                    "description": "",
                    "folderID": folderId,
                    "name": name,
                    "searchFilter": {
                        "familiesType": 0,
                        "folderObjectID": None,
                        "name":"Filter name",
                        "options":{
                            "isFamilyOptionsExpanded": True,
                            "isSheetSliderShown": False,
                            "version": "2.0"
                        },"parts": [],
                        "reportingDates": [21],
                        "reportingDate": 21,
                        "userCreated": "",
                        "userLastUpdated": "",
                        "clientID": "1",
                        "objectType": 7,
                        "lastAvailableData": None,
                        "searchMode": 1,
                        "searchSyntax":"Owner=(Apple)"
                    },
                    "shareWithReferencesRequest": {
                        "confirmShareReferences": False,
                            "referencedSharingObjects": [],
                            "sourceSharingObjectSettings": []
                        }
                }

    def create_simple_search_2(self, name, folderId):
        return {
                    "description": "",
                    "folderID": folderId,
                    "name": name,
                    "searchFilter": {
                        "familiesType": 0,
                        "folderObjectID": None,
                        "name":"Filter name",
                        "options": {
                            "isFamilyOptionsExpanded": True,
                            "isSheetSliderShown": False,
                            "version":"2.0"
                        },
                        "parts": [],
                        "reportingDates": [21],
                        "reportingDate": 21,
                        "userCreated": "", 
                        "userLastUpdated": "",
                        "clientID": "1",
                        "objectType": 7,
                        "lastAvailableData": None,
                        "searchMode": 1,
                        "searchSyntax":"(TAC=((transition metal% OR titanium OR ti OR titanocene OR zirconocene OR hafnocene OR zirconium OR zr OR hafnium OR hf OR *alumoxane* OR *aluminoxane* OR mao OR *boron OR *borane) and (metal ligand OR metallocen* OR cyclopentadien* OR fluoren* OR inden* OR activator) and (ethylen* OR polyolefin OR olefin* OR propylene) and (polymer* OR elastomer* OR copolymer* OR poe OR pop OR plastomer* OR interpolymer)) AND IPC=(C08F 4/6592 OR C08F4/659 OR C08F4/64 OR C08F 4/642 OR C07F 17/00 OR C07F 7/00 OR C08F 4/02 OR C08F 4/643 OR C08F 4/76 OR C08F 4/44 OR C07F 7/28 OR B01J 31/14 OR C08F 4/42 OR B01J 31/22 OR C08F 4/645 OR C08F 4/646 OR C08F 4/6392 OR C07F 7/10 OR C08F 4/602 OR C08F 4/619 OR C08F 4/639 OR C08F 4/00 OR B01J 31/12 OR B01J 31/16 OR C07F 5/02 OR C08F 4/16 OR B01J 31/00 OR B01J 37/00 OR C07F 5/00 OR C07F 9/00 OR C08F 2/34 OR C08F 4/6192 OR C08F 210/00 OR C08F 210/14 OR C08F 4/52 OR C08F 4/62 OR C08F 4/622 OR B01J 31/18 OR C07F5/06 OR C07F 7/08))\r\nAND NOT IPC=(C10G OR C07B 37/04 OR C07B 37/06 OR C07B 37/08 OR C07B 37/10 OR C07B 37/12 OR C07D 213/79 OR C08G 77 OR C07D 241 OR H01L OR B01J 29 OR A61K OR H01M OR A01N OR C07C 45 OR C08F 12 OR C08F 236 OR C08F 212 OR C07C 67 OR C07C 29 OR C07C 5 OR C07C 2 OR C08F 4/654 OR C07D 301 OR C07D 303 OR C07D 317/36 OR C08G 63/08 OR C07C 303 OR C08F 220 OR C08G 59 OR C07C 68) \r\nAND NOT (T=(RUBBER* OR graft polymer OR graft copolymer OR fluidized bed) OR \r\nC=((solid SEQ *catalyst) OR (supported SEQ2 catalyst) OR (polyolefin composition comprising)) OR TA=(fluidi?ed bed OR carrier supported OR gas phase reactor OR diene OR hydrogenated OR masterbatch OR polystyrene) OR TAC=((high SEQ density) OR polyoxyalkylene OR bimodal))"
                    },
                    "shareWithReferencesRequest": {
                        "confirmShareReferences": False,
                        "referencedSharingObjects": [],
                        "sourceSharingObjectSettings": []
                    }
                }
    
    def create_simple_referencing_search(self, name, folderId):
        return {
                    "description": "",
                    "folderID": folderId,
                    "name": name, 
                    "searchFilter": {
                        "familiesType": 0,
                        "folderObjectID": None,
                        "name":"Filter name",
                        "options": {
                            "isFamilyOptionsExpanded": True,
                            "isSheetSliderShown": False,
                            "version":"2.0"
                        },
                        "parts": [],
                        "reportingDates": [21],
                        "reportingDate": 21,
                        "userCreated": "",
                        "userLastUpdated": "",
                        "clientID": "1",
                        "objectType": 7,
                        "lastAvailableData": None,
                        "searchMode": 1,
                        "searchSyntax": "FilingDate=(2018)"
                    },
                    "shareWithReferencesRequest": {
                        "confirmShareReferences": False,
                        "referencedSharingObjects": [],
                        "sourceSharingObjectSettings":[]
                    }
                }

    def create_reference_two_searches(self, name, search1_ref_name, search2_ref_name, folderId):
        return {
                "description": "",
                "folderID": folderId,
                "name": name,
                "searchFilter": {
                    "familiesType": 0,
                    "folderObjectID": None,
                    "name": "Filter name",
                    "options": {
                        "isFamilyOptionsExpanded": True,
                        "isSheetSliderShown": False,
                        "version":"2.0"
                    },
                    "parts": [],
                    "reportingDates": [21],
                    "reportingDate": 21,
                    "userCreated": "",
                    "userLastUpdated": "",
                    "clientID": "1",
                    "objectType": 7,
                    "lastAvailableData": None,
                    "searchMode": 1,
                    "searchSyntax": "{} AND {}".format(search1_ref_name, search2_ref_name)
                },
                "shareWithReferencesRequest": {
                    "confirmShareReferences": False,
                    "referencedSharingObjects": [],
                    "sourceSharingObjectSettings":[]
                }
            }

    def create_fibonacci_reference_1(self, name, search_ref_name, folderId):
        return { 
            "description": "",
            "folderID": folderId,
            "name": name,
            "searchFilter": {
                "familiesType": 0,
                "folderObjectID": None,
                "name": "Filter name",
                "options": {
                    "isFamilyOptionsExpanded": True,
                    "isSheetSliderShown": False,
                    "version":"2.0"
                },
                "parts": [],
                "reportingDates": [21],
                "reportingDate": 21,
                "userCreated": "", 
                "userLastUpdated": "",
                "clientID": "1",
                "objectType": 7,
                "lastAvailableData": None,
                "searchMode": 1,
                "searchSyntax": "{}".format(search_ref_name)
            },"shareWithReferencesRequest": {
                "confirmShareReferences": False,
                "referencedSharingObjects": [],
                "sourceSharingObjectSettings": []
            }
        }

    def create_fibonacci_reference_2(self, name, search1_ref_name, search2_ref_name, folderId):
        return {
            "description": "",
            "folderID": folderId,
            "name": name,
            "searchFilter": {
                "familiesType": 0,
                "folderObjectID": None,
                "name":"Filter name",
                "options": {
                    "isFamilyOptionsExpanded": True,
                    "isSheetSliderShown": False,
                    "version":"2.0"
                },
                "parts": [],
                "reportingDates": [21],
                "reportingDate": 21,
                "userCreated": "",
                "userLastUpdated": "",
                "clientID": "1",
                "objectType": 7,
                "lastAvailableData": None,
                "searchMode": 1,
                "searchSyntax":"{} AND {}".format(search1_ref_name, search2_ref_name)
            },
            "shareWithReferencesRequest": {
                "confirmShareReferences": False,
                "referencedSharingObjects": [],
                "sourceSharingObjectSettings": []
            }
        }

    def create_fibonacci_reference_3(self, name, search1_ref_name, search2_ref_name, folderId):
        return {
            "description": "",
            "folderID": folderId,
            "name": name,
            "searchFilter": {
                "familiesType": 0,
                "folderObjectID": None,
                "name": "Filter name",
                "options": {
                    "isFamilyOptionsExpanded": True,
                    "isSheetSliderShown": False,
                    "version":"2.0"
                },
                "parts": [],
                "reportingDates": [21],
                "reportingDate": 21,
                "userCreated": "",
                "userLastUpdated": "",
                "clientID": "1",
                "objectType": 7,
                "lastAvailableData": None,
                "searchMode": 1,
                "searchSyntax": "{} AND {}".format(search1_ref_name, search2_ref_name)
            },
            "shareWithReferencesRequest": {
                "confirmShareReferences": False,
                "referencedSharingObjects": [],
                "sourceSharingObjectSettings": []
            }
        }

    
    def create_fibonacci_reference_4(self, name, search1_ref_name, search2_ref_name, folderId):
        return {
            "description": "",
            "folderID": folderId,
            "name": name,
            "searchFilter": {
                "familiesType": 0,
                "folderObjectID": None,
                "name":"Filter name",
                "options": {
                    "isFamilyOptionsExpanded": True,
                    "isSheetSliderShown": False,
                    "version":"2.0"
                },
                "parts": [],
                "reportingDates": [21],
                "reportingDate": 21,
                "userCreated": "",
                "userLastUpdated": "",
                "clientID": "1",
                "objectType": 7,
                "lastAvailableData": None,
                "searchMode": 1,
                "searchSyntax": "{} AND {}".format(search1_ref_name, search2_ref_name)
            },
            "shareWithReferencesRequest": {
                "confirmShareReferences": False,
                "referencedSharingObjects": [],
                "sourceSharingObjectSettings": []
            }
        }

    def create_fibonacci_reference_5(self, name, search1_ref_name, search2_ref_name, folderId):
        return {
            "description": "",
            "folderID": folderId,
            "name": name,
            "searchFilter": {
                "familiesType":0,
                "folderObjectID": None,
                "name": "Filter name",
                "options": {
                    "isFamilyOptionsExpanded": True,
                    "isSheetSliderShown": False,
                    "version":"2.0"
                },
                "parts": [],
                "reportingDates": [21],
                "reportingDate": 21,
                "userCreated": "",
                "userLastUpdated": "",
                "clientID": "1",
                "objectType": 7,
                "lastAvailableData": None,
                "searchMode": 1,
                "searchSyntax": "{} AND {}".format(search1_ref_name, search2_ref_name)
            },
            "shareWithReferencesRequest": {
                "confirmShareReferences": False,
                "referencedSharingObjects": [],
                "sourceSharingObjectSettings": []
            }
        }